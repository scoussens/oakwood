var ciModel;
var newModel;
var changes = [];
var pages = [
	{
		currentPage: "kcpl_create_gen_ci.html",
		currentType: "Create"
	},
	{
		currentPage: "kcpl_edit_gen_ci.html",
		currentType: "Edit"
	},
	{
		currentPage: "kcpl_create_td_ci.html",
		currentType: "Create"
	},
	{
		currentPage: "kcpl_edit_td_ci.html",
		currentType: "Edit"
	}
];
var selectedPage = 0;
var validator = null;
var formStatus = null;

//onReady function that loads each run
$(function () {
	$("#custom-form-nav div button").on('click', function() {
		var page = $(this).data("id");
		loadPage(pages[page]);
		selectedPage = page;
	});
});

function loadPage(page) {
	var currentPage = page.currentPage;
	var currentType = page.currentType;
	
	$('#custom-form-area').load('/CustomSpace/kcpl/' + currentPage, function(e){
		//initialize the form fields
		initForm();
		
		//add selection list to the top
		if(currentType === 'Edit'){
			loadList('#ci-select-list');
		}
		//if it's a create form then we don't have an existing model so we create our own 
		else if(currentType === 'Create') {
			var form = $('#ci-update-form');
			newModel = initModel(form);
		}
		//end
	});
}

//clears the form when no result is found (empty or invalid CI name)
function clearForm() {
	$('#ci-update-form input').each(function (){
		var inputName = $(this).attr("name");
		var type = $(this).attr("type");

		//check if input is an enum and init if so
		if(type === "enum") {
			var dropList = $('input[name='+ inputName +']').data("kendoDropDownList");
			dropList.select(0);
		} else if(type === "relationship") {
			var dropList = $('input[name='+ inputName +']').data("kendoDropDownList");
			dropList.select(0);
		} else {
			if(inputName != "Submit") {
				$('input[name='+ inputName +']').val("");
			}
		}
	});
}

//initializes the form, adding form controls and change events
function initForm() {
	//initialize the form fields
	$('#ci-update-form input[type=relationship]').each(function (){
		var inputName = $(this).attr("name");
		initRelationship($('input[name=' + inputName + ']'));
	});
	$('#ci-update-form input[type=childrelationship]').each(function (){
		var inputName = $(this).attr("name");
		initChildRelationship($('input[name='+ inputName +']'));
	});
	$('#ci-update-form input[type=enum]').each(function (){
		var inputName = $(this).attr("name");
		initEnum($('input[name=' + inputName + ']'));
	});
	$('#ci-update-form input[type=text]').each(function (){
		var inputName = $(this).attr("name");
		$('input[name='+ inputName +']').on("input", function(){ 
				newModel[inputName] = $(this).val(); 
				var index = changes.indexOf(inputName);
				if(index < 0){
					changes.push(inputName);
				}
			});
	});
	
	//add form validation
	validator = $('#ci-update-form').kendoValidator().data("kendoValidator");
	formStatus = $(".status");
	
	$("form").submit(function(event) {
		console.log("Submit button pressed, checking validation.");
		event.preventDefault();
		if(validator.validate()) {
			formStatus.text("Your request has been submitted.")
				.removeClass("invalid")
				.addClass("valid");
			createSR(newModel);
		} else {
			formStatus.text("Oops! There is invalid data in the form.")
				.removeClass("valid")
				.addClass("invalid");
			console.log("Invalid fields.");
		}
	});
}
//end

/* SR Creation Area
Here we perform all the actions required on submit to build the SR and submit it
with the changed data so it can be approved
*/
function buildDescription(cList, oModel, nModel, type) {
	var result = type + " CI Request Specifics: \n";
	for(var i=0; i < cList.length;i++) {
		if(type === "Edit") {
			result += cList[i] + ": " + oModel[cList[i]] + " > " + nModel[cList[i]] + "\n";
		} else if (type === "Create") {
			result += cList[i] + ": " + nModel[cList[i]] + "\n";
		}
	}
	
	return result;
}

function createSR (model) {
	//Logged in User Id
	var uid = session.user.Id;
	var udn = session.user.Name;
	
	//gets the current type of the sr request (edit/create)
	var type = pages[selectedPage].currentType;
	
	//Service Request Template ID for the default template we want to use
	//var templateId = '03bc9162-041f-c987-8ce4-a5547cd9ca04'; //Default Service Request
	var templateId = 'f6bcdbbf-80d9-af34-9bb1-894c761cb148' //KCPL.ServiceRequest.GenerationTD.Default *UPDATE ON INSTALL*
	
	//Serialize the form to use in automation
	var description = buildDescription(changes, ciModel, model, type);
	console.log(description);
	
	$.ajax({
		url: "/api/V3/Projection/CreateProjectionByTemplate",
		data: {id: templateId, createdById: uid},
		type: "GET",
		success: function (data) {
			console.log(data);

			//Create the new SR, filling in the required fields
			data.Title = "Configuration Item " + type +" Request";
			data.DisplayName = data.Id + ": " + data.Title;
			data.Description = description;
			data.RequestedWorkItem = [{
				ClassTypeId: "fb882d6b-bf4c-b78a-e1fe-f761114bc030",
				BaseId: uid,
				DisplayName: udn
			}];
			data.CreatedWorkItem = [{
				ClassTypeId: "fb882d6b-bf4c-b78a-e1fe-f761114bc030",
				BaseId: uid,
				DisplayName: udn
			}];
			
			//relates the configuration item if this is a edit request and the Ci already exists
			if(model.ClassId) {
				data.RelatesToConfigItem = [{
					ClassTypeId: model.ClassId,
					BaseId: model.BaseId,
					Id: model.BaseId
				}];
			}
			
			data.NameRelationship = [{
				Name: "RequestedWorkItem",
				RelationshipId: "DFF9BE66-38B0-B6D6-6144-A412A3EBD4CE"
			},
			{
				Name: "RelatesToWorkItem",
				RelationshipId: "cb6ce813-ea8d-094d-ee5a-b755701f4547"
			},
			{
				Name: "CreatedWorkItem",
				RelationshipId: "df738111-c7a2-b450-5872-c5f3b927481a"
			},
			{
				Name: "RelatesToConfigItem",
				RelationshipId: "d96c8b59-8554-6e77-0aa7-f51448868b43"
			}];
			data.Priority = {
				Id: "1e070214-693f-4a19-82bb-b88ee6362d98",
				Name: "Low",
				uid: "6f6e89f2-a506-43fb-9b2d-5cdcd44a84be"
			};   
			data.Urgency = {
				Id: "b02d9277-a9fe-86f1-e95e-0ba8cd4fd075",
				Name: "Low",
				uid: "3f3dcbe9-23aa-4f2b-8246-905faf287db6"  
			};
			//extension properties here specific to this utilization
			newModel["ProjectionId"] = $('#page-setup').data('projection-full');
			data.ext_ci_request_json = JSON.stringify(newModel);
			data.ext_ci_request_type = type;
			
			var strData = { "formJson":{"current": data }}
			
			$.ajax({
				url: "/api/V3/Projection/Commit",
				type: "POST",
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				data: JSON.stringify(strData) ,
				success: function (d) {
					console.log(data.Id + " created successfully.");
					window.location.href = window.location.protocol + "//" + window.location.hostname + "/ServiceRequest/Edit/" + data.Id
				}
			});
			
		}
	});
}
//end