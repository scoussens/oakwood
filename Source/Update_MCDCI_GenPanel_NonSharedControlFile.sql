
Update ViewPanel
SET Definition =

'{
"id":"c69158f0-3743-4c3b-8904-fe7ba6026cc5",
"type":"html",
"content":"
<div>
<head>
	<title>MCDCI T&D</title>
	<link rel=\"stylesheet\" type=\"text/css\" href=\"/CustomSpace/MCDCIShared/kcpl.css\">
	<script src=\"/CustomSpace/MCDCIGEN/kcpl.js\"></script>
	<script src=\"/CustomSpace/MCDCIGEN/kcpl_controls.js\"></script>
</head>
<div id=\"custom-form-nav\">
	<div class=\"btn-group\" role=\"group\" aria-label=\"...\">
		<button type=\"button\" class=\"btn btn-default\" data-id=\"0\">Asset Create</button>
		<button type=\"button\" class=\"btn btn-default\" data-id=\"1\">Asset Modify</button>
		<button type=\"button\" class=\"btn btn-default\" data-id=\"2\">Asset Retire</button>
		<button type=\"button\" class=\"btn btn-default\" data-id=\"3\">Initiate Change Request</button>
	</div>
</div>
<div id=\"custom-form-area\"></div>
</div>
"}'

where Id = 'c69158f0-3743-4c3b-8904-fe7ba6026cc5'