
$guid = "77994bbe-df0b-55ce-1ffb-851dd7139fcc"
$debug = $true
try{
	if (!(Get-Module -Name SMLets)) { Import-Module SMLets -ErrorAction Stop }

	#get the SR object that contains the json extensions
	$sr = Get-SCSMObject -Id $guid

	#get the form data and convert it to an object, along with type (edit/create)
	$ci = ConvertFrom-Json -InputObject $sr.ext_ci_request_json
	$type = $sr.ext_ci_request_type

	#setup primary object hash
	$params = @{}
	#setup relationship hash
	$relations = @{}
	#setup enumerations hash
	$enums = @{}

	#setup exclude properties table
	$exclude = @(
		"ClassTypeId",
		"ProjectionId"
	)
	if($type -eq 'Edit')
	{
		$exclude += @(
		"AssetStatus",
		"AssetUniqueID",
		"BaseId",
		"ClassName",
		"FullName",
		"NameRelationship",
		"ObjectStatus",
		"LastModified",
		"LastModifiedBy",
		"TimeAdded"
		)
	}

	#retrieve projection id from object type
	$projectionId = $ci.ProjectionId
	$ciProjectionName = (Get-SCSMTypeProjection | ?{$_.Id -eq $projectionId}).Name

	#debug
    if($debug = $true){
	    #Write-Host -ForegroundColor Red $ciProjectionName
    }

	function checkExclude {
		param($name)

		foreach($item in $exclude) {
			if($name -eq $item) {
				return $true
			}
		}

		return $false
	}

	function checkType {
		param($obj)
		$type = $null

		if($obj.Id)
		{
			$type = "enum"
		}
		elseif($obj.BaseId)
		{
			$type = "relation"
		}

		return $type
	}

	function addEnumerationsToParams {
		param($Parameters, $Enumerations)

		foreach($key in $Enumerations.Keys) {
            if($Enumerations[$key].length -gt 0){
				$Parameters.Add($key,(Get-SCSMEnumeration -Id $Enumerations[$key]))
            }
            else {
                $Parameters.Add($key,$null)
            }
		}

		return $Parameters
	}

	function createObject {
		param($Parameters, $ClassId, $ProjectionName, $Relationships, $Enumerations)
    
		$class = Get-SCSMClass -Id $ClassId
        
        #debug
        if($debug = $true){
            #Write-Host "ClassName: "$class.Name
        }
		
        #add all of the enumerations to the parameters list
		$Parameters = addEnumerationsToParams  -Parameters $Parameters -Enumerations $Enumerations
            
        #debug
        if($debug = $true){
            #Write-Host "Parameters to be inserted:"
            foreach($key in $Parameters.Keys){
                #Write-Host "`t" $key " : " $Parameters[$key]
            }
        }

		#create the object shell/nocommit so we have the GUID and other information using our parameters
		$object = New-SCSMObject -Class $class -PropertyHashtable $Parameters -NoCommit
        $objectId = $object.get_id()
            
        #debug
        if($debug = $true){
            $object | fl
        }

		#create an array of relationship objects targeting the required targets and then iterate through and commit each of them
		$relationshipObjects = @()
		foreach($key in $Relationships.Keys) {
            if($key.StartsWith("Target_")){
			    $relationshipName = (Get-SCSMRelationshipClass | Select-Object -ExpandProperty Target | ?{$_.Name -eq $key}).ParentElement
			    $relationshipClass = Get-SCSMRelationshipClass -Name $relationshipName$
                
                #debug
                if($debug = $true){
                    #Write-Host "Relationship Type: Target_"
                    #Write-Host "Key Name:" $key
                    #Write-Host "Relationship Name:" $relationshipName
                    $relationshipClass | fl
                }

                #retreive the target object and create a new relationship, but don't commit yet
			    $targetObject = Get-SCSMObject -Id $Relationships[$key].BaseId
                
                #debug
                if($debug = $true){
                    #Write-Host "Target ID:" $targetObject.get_id()
                    #Write-Host "Source ID:" $object.get_id()
                }
			    
                $relationshipObjects += New-SCSMRelationshipObject -Relationship $relationshipClass -Source $object -Target $targetObject -NoCommit
            }
            #this is a check to see if the relationship end points is flipped from what you would expect
            elseif ($key.StartsWith("Source_")){
                $relationshipName = (Get-SCSMRelationshipClass | Select-Object -ExpandProperty Source | ?{$_.Name -eq $key}).ParentElement
			    $relationshipClass = Get-SCSMRelationshipClass -Name $relationshipName$
                
                #debug
                if($debug = $true){
                    #Write-Host "Relationship Type: Source_"
                    #Write-Host "Key Name:" $key
                    #Write-Host "Relationship Name:" $relationshipName
                    $relationshipClass | fl
                }

                #retreive the target object and create a new relationship, but don't commit yet
			    $targetObject = Get-SCSMObject -Id $Relationships[$key].BaseId

                #debug
                if($debug = $true){
                    #Write-Host "Target ID:" $targetObject.get_id()
                    #Write-Host "Source ID:" $object.get_id()
                }

			    $relationshipObjects += New-SCSMRelationshipObject -Relationship $relationshipClass -Source $targetObject -Target $object -NoCommit
            }
		}
		foreach($relObject in $relationshipObjects) {
            #debug
            if($debug = $true){
                $relObject | fl
            }

			$relObject.Commit()
		}
	}

	function updateObject {
		param($Parameters, $ClassId, $Relationships, $Enumerations, $Id)
    
		$class = Get-SCSMClass -Id $ClassId

		#add all of the enumerations to the parameters list
		$Parameters = addEnumerationsToParams  -Parameters $Parameters -Enumerations $Enumerations
		#update the object
		$object = Get-SCSMObject -Id $Id
		$object | Set-SCSMObject -PropertyHashtable $Parameters
        
        #debug
        if($debug -eq $true) {
            #Write-Host "Setting Relationships"
        }
		foreach($key in $Relationships.Keys) {
            if($key.StartsWith("Target_")){
			    $relationshipName = (Get-SCSMRelationshipClass | Select-Object -ExpandProperty Target | ?{$_.Name -eq $key}).ParentElement
			    $relationshipClass = Get-SCSMRelationshipClass -Name $relationshipName$
			    $relationshipId = $relationshipClass.Id
                
                #debug
                if($debug = $true){
                    #Write-Host "Relation Type: Target_"
                    #Write-Host "Key Name:" $key
                    #Write-Host $relationshipName
                }
			    
                $targetObject = Get-SCSMObject -Id $Relationships[$key].BaseId

			    Get-SCSMRelationshipObject -BySource $object | ?{$_.RelationshipId -eq $relationshipId} | Remove-SCSMRelationshipObject
			    New-SCSMRelationshipObject -Relationship $relationshipClass -Source $object -Target $targetObject -Bulk
            }
            elseif($key.StartsWith("Source_")){
                $relationshipName = (Get-SCSMRelationshipClass | Select-Object -ExpandProperty Source | ?{$_.Name -eq $key}).ParentElement
			    $relationshipClass = Get-SCSMRelationshipClass -Name $relationshipName$
			    $relationshipId = $relationshipClass.Id
                
                #debug
                if($debug = $true){
                    #Write-Host "Relation Type: Source_"
                    #Write-Host "Key Name:" $key
                    #Write-Host $relationshipName
                }

			    $targetObject = Get-SCSMObject -Id $Relationships[$key].BaseId

			    Get-SCSMRelationshipObject -ByTarget $object | ?{$_.RelationshipId -eq $relationshipId} | Remove-SCSMRelationshipObject
			    New-SCSMRelationshipObject -Relationship $relationshipClass -Source $targetObject -Target $object -Bulk
            }
		}

	}

	#iterate through the array to pull out the object values
	foreach($item in ($ci | get-member -MemberType NoteProperty).Name ) {
		$excluded = checkExclude -name $item
		if($excluded -ne $true) {
			$value = $ci.$item
			#catch any sub objects
			if($value -and ($value.GetType()).Name -eq 'PSCustomObject' ) 
			{
				#debug
                if($debug = $true){
				    #Write-Host -ForegroundColor Gray $item
                }
            
				#check if it's a relationship or enumeration
				$objType = checkType($value)
				if($objType -eq "relation")
				{
					$relations.Add($item,@{
						"BaseId" = $value.BaseId;
						"ClassId" = $value.ClassId;
					})
				}
				elseif($objType -eq "enum")
				{
					$enums.Add($item,$value.Id)
				}
            
				#debug
                if($debug = $true){
				    foreach( $subItem in ($value | Get-Member -MemberType NoteProperty).Name ) 
				    {
					    $subValue = $value.$subItem
					    #Write-Host -ForegroundColor Yellow "`t"$subItem ": " $subValue
				    }
				}
			} 
			else 
			{
				$params.Add($item,$value)

				#debug
                if($debug = $true){
				    #Write-Host -ForegroundColor Green $item ": " $value "Length:" $value.Length
                }
			}
		}
	}

	if($type -eq 'Create') {
        #debug
        if($debug = $true){
		    #Write-Host -ForegroundColor Yellow "Creating object..."
        }

		createObject -Parameters $params -ClassId $ci.ClassTypeId -Relationships $relations -Enumerations $enums
		
        #debug
        if($debug = $true) {
            #Write-Host -ForegroundColor Yellow "Object created."
        }
	}
	elseif($type -eq 'Edit') {
        #debug
        if($debug = $true) {
		    #Write-Host -ForegroundColor Yellow "Updating object..."
        }
		
        updateObject -Parameters $params -ClassId $ci.ClassTypeId -Relationships $relations -Enumerations $enums -Id $ci.BaseId
		
        #debug
        if($debug = $true) {
            #Write-Host -ForegroundColor Yellow "Object updated."
        }
	}
}
catch {
	throw($_ | fl * | Out-String)
}
