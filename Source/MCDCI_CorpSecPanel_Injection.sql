--v6.0.0.0
--use a GUID generator to create some new guids for your view panel and navigation node.  NOTE: All guids must be completely lower case.
DECLARE @NavNodeGuid nvarchar(255)                  = '2b5e916e-9e03-476e-a46d-52073bc286e6' 
DECLARE @ViewPanelGuid nvarchar(255)                = 'b63469f0-5682-4c3b-8904-fe7ba6029bb7'

-- replace these values with what you want
DECLARE @Url nvarchar(max)               = '../MCDCICORPSEC/Edit_CI.html'
DECLARE @MenuTitle nvarchar(255)  = 'MCDCI CorpSec'
DECLARE @Locale nvarchar(3)              = 'ENU'                                                             

--this just deletes it if it already exists so that you can iteratively tweak it and recreate it easily
delete from DisplayString where ElementID = @NavNodeGuid and LocaleID = @Locale
delete from NavigationNode where Id = @NavNodeGuid
delete from ViewPanel where id = @ViewPanelGuid

--this creates the navigation node display string
INSERT INTO [dbo].[DisplayString]  (ElementID, LocaleID, DisplayString) 
VALUES (@NavNodeGuid, @Locale, @MenuTitle)

--Create the navigation node.
--This example creates a navigation node with only one row/one column with a single view panel
INSERT INTO NavigationNode(Id, [Definition], Ordinal, Sealed, IsPublic, IsVisible, LicenseRequired, IconClass)
VALUES (
@NavNodeGuid, 
'{"Id":"' + @NavNodeGuid + '","layoutType":"full","view":{"header":{"title":"Corporate Security: Master Critical Device CIs","subTitle":""},"body":{"content":{"rows":[{"columns":[{"ColSpan":12,"type":"viewPanel","ViewPanelId":"' + @ViewPanelGuid + '"}]}]}}}}',
0,1,0,0,NULL, 'fa fa-briefcase')

--Create the view panel
--This example defines a type=HTML view panel that fills up the entire view panel port and embeds an iframe pointed at the URL specified.  
--You can include HTML using an iframe like this example or you can hard code any HTML/Javascript in the view panel content attribute.
INSERT INTO ViewPanel(Id, [Definition], TypeId)
VALUES (
@ViewPanelGuid,
'{
"id":"' + @ViewPanelGuid + '",
"type":"html",
"content":"
<div>
<head>
	<title>MCDCI Gen</title>
	<link rel=\"stylesheet\" type=\"text/css\" href=\"/CustomSpace/MCDCIShared/kcpl.css\">
	<script src=\"/CustomSpace/MCDCICorpSec/kcpl.js\"></script>
	<script src=\"/CustomSpace/MCDCICorpSec/kcpl_controls.js\"></script>
</head>
<div id=\"custom-form-nav\">
	<div class=\"btn-group\" role=\"group\" aria-label=\"...\">
		<button type=\"button\" class=\"btn btn-default\" data-id=\"0\">Asset Create</button>
		<button type=\"button\" class=\"btn btn-default\" data-id=\"1\">Asset Modify</button>
		<button type=\"button\" class=\"btn btn-default\" data-id=\"2\">Asset Retire</button>
		<button type=\"button\" class=\"btn btn-default\" data-id=\"3\">Initiate Change Request</button>
	</div>
</div>
<div id=\"custom-form-area\"></div>
</div>
"}',
'html'
)