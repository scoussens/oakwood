KCPL Custom Form Readme

Requirements:
	KCPL.GenerationTD.Library.mp : KCPL Generation TD Class Library (Sealed) 
    Cireson Portal : v3.8 (tested)
    System Center Orchestrator 2012R2 : Service Manager IP 1.0+ (installed)
        *NOTE: Orchestrator must be configured to run the SCSM SMLets natively or the runbook script will need to be modified.
    
Solution Components:
    Service Manager:
        KCPL.GenerationTD.ProjectionLibrary.mp (Sealed)
        KCPL.GenerationTD.SR.Extensions.mp (Sealed)
        KCPL.GenerationTD.CI.Template.Library.xml (Unsealed)
    SQL (ServiceManagement DB):
        ViewPanel_Injection.sql
    Orchestrator:
        KCPL_SCOR_Create_Update_RB.ois_export
    Cireson Portal:
        KCPL Folder (and contents)
            kcpl.css
            kcpl.js
            kcpl_controls.js
            kcpl_create_gen_ci.html
            kcpl_edit_gen_ci.html
            kcpl_create_td_ci.html
            kcpl_edit_rd_ci.html
            
Installation:
First: We should install the portal components into the customspace folder. To do this, copy the KCPL folder and it's contents into the CiresonPortal\CustomSpace folder. This should leave you with CiresonPortal\CustomSpace\KCPL\(contents). This is the all of the javascript and html pages that are required. As long as they are in this folder they will not be modified or overwritten on portal upgrade.

Second: We will install the required management packs. For this we are assuming that the KCPL class management packs are already installed and that they were sealed so the GUIDs should all be identical.

Import the three management packs (two mp and one xml) from the MP folder. The SR class extensions should be imported first but can be imported at the same time as the Projection library. After both of those are successfully imported we need to import the Template Library .xml.

At this point we can restart the Cireson Portal cachebuilder service so it picks up the new type projections and the SR class extensions.

Third: We want to install the navigation nodes and the viewpanel code into the portal. 

NOTE: this will be overwritten on upgrade and will need to be re-run after an upgrade to recreate the navigation and viewpanel.

We will need to run the .sql query that is in the SQL folder of the zip file against the ServiceManagement (not ServiceManager) database (Normally located on the SCSM DB server and instance). This will create the navigation node and viewpanel information, including the HTML that loads the buttons and the javascript.

Once that is complete we will need to log into the portal as an administrator and open the Navigation Settings for the portal. Here we will find our new navigation node and set the permissions on it. We can also nest it in another folder if necessary but it will originally show at the root level.

From this point you should be able to select the new navigation node and see the custom HTML page. You should also be able to click the buttons on the page and load the appropriate HTML form. All of the enumerations and relationships SHOULD work at this point.

Fourth: We need to setup the Orchestrator component in order to actually update the configuration items and create new configuration items after SR approval.

To do this we need to log into Orchestrator and import the runbook export from the RB folder. Verify published data variables in the script maintained their values to the initialize activity.

Now run the Orchestrator connector in Service Manager.

Once it finishes running successfully verify that the new runbook exists in the Library\Runbooks section of Service Manager.

Open the new KCPL runbook template that we imported and verify that it maintained it's relationship to the runbook we just imported. If it didn't, we will need to reassociate it or possibly recreate it.

Open the new SR template we imported and verify it's runbook activity maintained it's association and values, and if not then recreated it from our newly updated/created runbook template (the guid should point at the parent SR id property).

We need to get the GUID of the SR template in your environment (it was not sealed so the GUID will change on import). To do this we can log into the orchestrator server again open powershell and run the following commands (assuming your SCOR server is configured as required at the top of this readme):

import-module smlets

Get-SCSMObjectTemplate KCPL.ServiceRequest.GenerationTD.Default | select Name, Id

Copy the value of the ID property to the clipboard.

Log into the Portal web server and open the kcpl.js file in the CustomSpace\KCPL folder. Scroll to the bottom and find the following entry and update the GUID with the new GUID you just copied.

	//Service Request Template ID for the default template we want to use
	//var templateId = '03bc9162-041f-c987-8ce4-a5547cd9ca04'; //Default Service Request
	var templateId = 'f6bcdbbf-80d9-af34-9bb1-894c761cb148' <---- UPDATE THIS GUID
    
Save the kcpl.js file and this completes your installation.