--use a GUID generator to create some new guids for your view panel and navigation node.  NOTE: All guids must be completely lower case.
DECLARE @NavNodeGuid nvarchar(255)                  = '9e0e71d2-d8ef-400a-885a-b4d74d4a1963' 
DECLARE @ViewPanelGuid nvarchar(255)                = '277a30d5-cb1f-42e6-bf60-4978742d56ca'

-- replace these values with what you want
DECLARE @Url nvarchar(max)               = '../KCPL/Edit_CI.html'
DECLARE @MenuTitle nvarchar(255)  = 'Configuration Item Requests'
DECLARE @Locale nvarchar(3)              = 'ENU'                                                             

--this just deletes it if it already exists so that you can iteratively tweak it and recreate it easily
delete from DisplayString where ElementID = @NavNodeGuid and LocaleID = @Locale
delete from NavigationNode where Id = @NavNodeGuid
delete from ViewPanel where id = @ViewPanelGuid

--this creates the navigation node display string
INSERT INTO [dbo].[DisplayString]  (ElementID, LocaleID, DisplayString) 
VALUES (@NavNodeGuid, @Locale, @MenuTitle)

--Create the navigation node.
--This example creates a navigation node with only one row/one column with a single view panel
INSERT INTO NavigationNode(Id, [Definition], Ordinal, Sealed, IsPublic, IsVisible, LicenseRequired, IconClass)
VALUES (
@NavNodeGuid, 
'{"Id":"' + @NavNodeGuid + '","layoutType":"full","view":{"header":{"title":"Edit CI Request","subTitle":""},"body":{"content":{"rows":[{"columns":[{"ColSpan":12,"type":"viewPanel","ViewPanelId":"' + @ViewPanelGuid + '"}]}]}}}}',
0,1,0,0,NULL, 'fa fa-briefcase')

--Create the view panel
--This example defines a type=HTML view panel that fills up the entire view panel port and embeds an iframe pointed at the URL specified.  
--You can include HTML using an iframe like this example or you can hard code any HTML/Javascript in the view panel content attribute.
INSERT INTO ViewPanel(Id, [Definition], TypeId)
VALUES (
@ViewPanelGuid,
'{
"id":"' + @ViewPanelGuid + '",
"type":"html",
"content":"
<div>
<head>
	<title>Configuration Item Request</title>
	<link rel=\"stylesheet\" type=\"text/css\" href=\"/CustomSpace/kcpl/kcpl.css\">
	<script src=\"/CustomSpace/kcpl/kcpl.js\"></script>
	<script src=\"/CustomSpace/kcpl/kcpl_controls.js\"></script>
</head>
<div id=\"custom-form-nav\">
	<div class=\"btn-group\" role=\"group\" aria-label=\"...\">
		<button type=\"button\" class=\"btn btn-default\" data-id=\"0\">Gen Create</button>
		<button type=\"button\" class=\"btn btn-default\" data-id=\"1\">Gen Edit</button>
		<button type=\"button\" class=\"btn btn-default\" data-id=\"2\">TD Create</button>
		<button type=\"button\" class=\"btn btn-default\" data-id=\"3\">TD Edit</button>
	</div>
</div>
<div id=\"custom-form-area\"></div>
</div>
"}',
'html'
)
