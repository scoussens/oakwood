--use a GUID generator to create some new guids for your view panel and navigation node.  NOTE: All guids must be completely lower case.
DECLARE @NavNodeGuid nvarchar(255)                  = '7c276b4e-14c5-4ad0-9942-f62502435362' 
DECLARE @ViewPanelGuid nvarchar(255)                = 'c73f87d2-df3a-4419-adf7-862c2b7b7930'

-- replace these values with what you want
DECLARE @Url nvarchar(max)               = '../MCDCITD/Edit_CI.html'
DECLARE @MenuTitle nvarchar(255)  = 'MCDCI T&D'
DECLARE @Locale nvarchar(3)              = 'ENU'                                                             

--this just deletes it if it already exists so that you can iteratively tweak it and recreate it easily
delete from DisplayString where ElementID = @NavNodeGuid and LocaleID = @Locale
delete from NavigationNode where Id = @NavNodeGuid
delete from ViewPanel where id = @ViewPanelGuid

--this creates the navigation node display string
INSERT INTO [dbo].[DisplayString]  (ElementID, LocaleID, DisplayString) 
VALUES (@NavNodeGuid, @Locale, @MenuTitle)

--Create the navigation node.
--This example creates a navigation node with only one row/one column with a single view panel
INSERT INTO NavigationNode(Id, [Definition], Ordinal, Sealed, IsPublic, IsVisible, LicenseRequired, IconClass)
VALUES (
@NavNodeGuid, 
'{"Id":"' + @NavNodeGuid + '","layoutType":"full","view":{"header":{"title":"T&D: Master Critical Device CIs","subTitle":""},"body":{"content":{"rows":[{"columns":[{"ColSpan":12,"type":"viewPanel","ViewPanelId":"' + @ViewPanelGuid + '"}]}]}}}}',
0,1,0,0,NULL, 'fa fa-briefcase')

--Create the view panel
--This example defines a type=HTML view panel that fills up the entire view panel port and embeds an iframe pointed at the URL specified.  
--You can include HTML using an iframe like this example or you can hard code any HTML/Javascript in the view panel content attribute.
INSERT INTO ViewPanel(Id, [Definition], TypeId)
VALUES (
@ViewPanelGuid,
'{
"id":"' + @ViewPanelGuid + '",
"type":"html",
"content":"
<div>
<head>
	<title>MCDCI T&D</title>
	<link rel=\"stylesheet\" type=\"text/css\" href=\"/CustomSpace/MCDCITD/kcpl.css\">
	<script src=\"/CustomSpace/MCDCITD/kcpl.js\"></script>
	<script src=\"/CustomSpace/MCDCITD/kcpl_controls.js\"></script>
</head>
<div id=\"custom-form-nav\">
	<div class=\"btn-group\" role=\"group\" aria-label=\"...\">
		<button type=\"button\" class=\"btn btn-default\" data-id=\"0\">Asset Create</button>
		<button type=\"button\" class=\"btn btn-default\" data-id=\"1\">Asset Modify</button>
		<button type=\"button\" class=\"btn btn-default\" data-id=\"2\">Asset Retire</button>
		<button type=\"button\" class=\"btn btn-default\" data-id=\"3\">Initiate Change Request</button>
	</div>
</div>
<div id=\"custom-form-area\"></div>
</div>
"}',
'html'
)