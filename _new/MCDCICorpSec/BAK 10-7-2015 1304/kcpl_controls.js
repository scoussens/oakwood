/*
we are going to read the class = enum, 
iterate through the class and get the id, 
use the id to get the datasource,
then create an enum drop down off that.
this is a temprorary datasource
*/

//loads the selected list and setups up the onChange function
function loadList (selector, textField, valueField, isParent, listType, listTemplate, tFilter, cFilter, pFilter, oFilter, vFilter) {

	var list = $(selector);
	//debug
	//console.log("Entering loadList...", list);
	//console.log("List template...", listTemplate);
	
	//setup criteria properties from HTML
	var projectionId = list.data("projection");


	var classId = list.data("class");
	var childList = list.data("cascade-target");
	var targetRel = list.data("target-filter-relationship");
	var targetPropId = list.data("target-filter-property-id");
	var targetOperator = list.data("target-filter-property-operator");
	var targetProp = list.data("target-filter-property-name");
	
	//setup the data text fields
	var textField = typeof textField != "undefined" ? textField : list.attr("name");
	var valueField = typeof valueField != "undefined" ? valueField : "BaseId";
	var listTemplate = typeof listTemplate != "undefined" ? listTemplate : null;
	var isParent = typeof isParent == "boolean" ? isParent : false;
	var listType = list.data("list-type") == null ? "kendoComboBox" : list.data("list-type");
	
	//console.log(listType);
	
	//sets the secondary filter properties if they are not passed in
	var filterType = typeof tFilter != "undefined" ? tFilter : list.data("filter-type");
	var filterClass = typeof cFilter != "undefined" ? cFilter : list.data("filter-class");
	var filterProperty = typeof pFilter != "undefined" ? pFilter : list.data("filter-property");
	var filterOperator = typeof oFilter != "undefined" ? oFilter : list.data("filter-operator");
	var filterValue = typeof vFilter != "undefined" ? vFilter : list.data("filter-value");
	
	//sets the expression to use (NOTE: this could be incorporated into the criteria statements below for reduced code)
	var secondLeftExpression = null;
	var secondOperator = null;
	var secondRightExpression = null;
	if(filterType == "relationship" && filterClass){
		//console.log("Filtering by relationship..");
		secondLeftExpression = "$Context/Path[Relationship='"+targetRel+"' SeedRole='Source' TypeConstraint='"+filterClass+"']/Property[Type='"+filterClass+"']/"+filterProperty+"$";
		secondOperator = filterOperator;
		secondRightExpression = filterValue;
	} else if (filterType == "property") {
		//console.log("Filtering by property..");

		secondLeftExpression = "$Context/Property[Type='" + classId + "']/" + filterProperty + "$";;
		secondOperator = filterOperator;
		secondRightExpression = filterValue;
	}
	
	var criteria;
	if(filterType) {
		criteria = {
			"Id": projectionId, "Criteria": {"Base": {"Expression": {



				"And": {
					"Expression": [
						{"SimpleExpression": {
								"ValueExpressionLeft": {"GenericProperty": "Id"},


								"Operator": "Like",
								"ValueExpressionRight": {"Value": "%%"}


							}
						},


						{"SimpleExpression": {
								"ValueExpressionLeft": {"Property": secondLeftExpression},


								"Operator": secondOperator,
								"ValueExpressionRight": {"Value": secondRightExpression}



							}

						}
					]
			}}}}
		}
	} else {
		criteria = {
			"Id": projectionId,
			"Criteria": {
				"Base": {
					"Expression": {
						"SimpleExpression": {
							"ValueExpressionLeft": {
								"GenericProperty": "Id"
							},
							"Operator": "Like",
							"ValueExpressionRight": {
								"Value": "%%"
							}
						}
					}
				}
			}

		};
	}


	
	//reads in the datasource if the list requires it :default:

	var dataSource = null;
	dataSource = new kendo.data.DataSource({
		transport: {
			read: {
				type: "POST",
				url: "/api/V3/Projection/GetProjectionByCriteria",






				contentType: 'application/json',
				dataType: 'json',
				data: criteria
			},
			parameterMap: function (data) {
				return JSON.stringify(data)
			}
		},
		sort: {
			field: textField,
			dir: "asc"
		}
	});
				
	//creates the kendo list item
	//console.log("Fields:" + textField + "|" + valueField);
	
	if(listType == "kendoComboBox") {
		list.kendoComboBox({
			dataTextField: textField,
			dataValueField: valueField,
			template: listTemplate,
			dataSource: dataSource,
			index: -1,
			height: 400,
			suggest: false,
			filter: "contains",
			minLength: 3,
			change: childList ? enableChildList : selectionChange,
			dataBound: childList ? enableChildList : selectionChange,
			autoBind: false,
			autoClose: false,
			enable: childList ? true : false
		});
	} else if (listType == "kendoMultiSelect"){
		list.kendoMultiSelect({
			placeholder: "Select items...",
			dataTextField: textField,
			dataValueField: valueField,
			itemTemplate: listTemplate,
			dataSource: dataSource,
			height: 400,
			change: childList ? enableChildList : selectionChange,
			dataBound: childList ? enableChildList : selectionChange,
			autoBind: false,
			autoClose: false,
			enable: childList ? true : false,
			select: function (e) {
				if (pages[selectedPage].currentType === 'CR'){
					//we use this action if it's a CR we are editing
					//console.log('makes changes for a CR');
					
					if (newModel) { //if it doesn't exist yet (new item) then we create it on the model
					newModel["RelatesToConfigItem"] = this.dataItems();
					} else { //this is a catch if neither of the above works for some reason
						//console.log("Error: Model does not exist.");
					}
					
					//check the global change array to see if this change has already been tracked, if not then track it
					if(changes.indexOf(name) < 0){
						changes.push(name);
					}
				}
			}
		});
	}



	
	//function for the selection CHANGE action
	function selectionChange (e) {
		var dataItem = this.dataItem(e.item);
		if(typeof dataItem != "undefined" && dataItem[valueField] != null) {
			var criteria = {

				"Id": 'fb33ac1b-8b90-2282-4d79-6d68e22dc33c',
				"Criteria": {"Base": {"Expression": {"SimpleExpression": {



								"ValueExpressionLeft": {"GenericProperty": "Id"},


								"Operator": "Equal",
								"ValueExpressionRight": {"Value": dataItem[valueField] }








			}}}}};
			$.ajax({
				url: "/api/V3/Projection/GetProjectionByCriteria",

				data: JSON.stringify(criteria),
				type: "POST",
				dataType: "json",
				contentType: 'application/json; charset=UTF-8',
				success: function (data) {		
					if(pages[selectedPage].currentType === 'Edit') {
					//we use this action for edit and retire forms
						if(typeof data === 'string') {
							//console.log("No results returned or other error.");
							clearForm();
						} else {
							var jsonData = JSON.stringify(data[0]);
							ciModel = JSON.parse(jsonData);
							newModel = JSON.parse(jsonData);
							fillForm(ciModel);
						}





































					}










				}
			});
		};
	};
	function enableChildList (e) {
		//these values come from the parent list / initialization
		var selector = "#" + childList;
		var pList = e.sender;
		var listType = $(selector).data("list-type");
		var list = null;
		
		//checks to see if we defined the type of list this should be or if it's null we use the default
		if(listType == null || listType == "kendoComboBox"){
			list = $(selector).data("kendoComboBox");
		} else if (listType == "kendoMultiSelect"){
			list = $(selector).data("kendoMultiSelect");
		}
		
		//console.log(selector, list);
		
		if(pList.selectedIndex == -1){
			return;
		}
		
		var parentClassId = classId;
		var childClassId = $(selector).data("class");

		
		var projection = $(selector).data("projection");
		
		//sets up the parents filter properties
		var filterRelationship = targetRel
		var filterProperty = targetPropId;
		var filterOperator = targetOperator;
		var filterValue = targetProp;
		var filterObj = pList.dataItem();
		//console.log("Filter value: ", filterValue);
		//console.log("Filter object: ", filterObj);
		
		//sets up any required additional filters for the child list that aren't passed from the parent
		var childFilterType = $(selector).data("filter-type");
		var childFilterClass = $(selector).data("filter-class");
		var childFilterRelationship = $(selector).data("filter-relationship");
		var childFilterProperty = $(selector).data("filter-property");
		var childFilterOperator = $(selector).data("filter-operator");
		var childFilterValue = $(selector).data("filter-value");

		//console.log("Entered child enabler: " + selector);





		
		//console.log("Entered child enabler: " + selector);
		//var list = $(selector).data("kendoComboBox");
		if(list) {
			var dataItem = this.dataItem(e.item);
			var criteria = null
			if(childFilterType == 'relationship'){
				criteria = {
				"Id": projection,
				"Criteria": {"Base": {"Expression": {"And": {
					"Expression": [ 
						{"SimpleExpression": {
							"ValueExpressionLeft": {
								"Property": "$Context/Path[Relationship='"+filterRelationship+"' SeedRole='Source' TypeConstraint='"+parentClassId+"']/Property[Type='"+parentClassId+"']/"+filterProperty+"$"},
							"Operator": filterOperator,
							"ValueExpressionRight": {"Value": filterObj[filterValue]}
							}
						},
						{"SimpleExpression": {
							"ValueExpressionLeft": {
								"Property": "$Context/Path[Relationship='"+childFilterRelationship+"' SeedRole='Source' TypeConstraint='"+childFilterClass+"']/Property[Type='"+childFilterClass+"']/"+childFilterProperty+"$"},
							"Operator": childFilterProperty,
							"ValueExpressionRight": {"Value": childFilterValue}
							}
						}
					]
				}}}}}
			} else {
				criteria = {
				"Id": projection,
				"Criteria": {"Base": {"Expression": {"And":{
					"Expression":[
						{"SimpleExpression": {
							"ValueExpressionLeft": {
								"Property": "$Context/Path[Relationship='"+filterRelationship+"' SeedRole='Source' TypeConstraint='"+parentClassId+"']/Property[Type='"+parentClassId+"']/"+filterProperty+"$"},
							"Operator": filterOperator,
							"ValueExpressionRight": {"Value": filterObj[filterValue]}
							}
						},
						{"SimpleExpression": {
								"ValueExpressionLeft": {"Property": "$Context/Property[Type='"+childClassId+"']/"+childFilterProperty+"$"},
								"Operator": childFilterOperator,
								"ValueExpressionRight": {"Value": childFilterValue}
							}
						}
					]
				}}}}}
			};
			
			//console.log("Criteria", criteria);
			

			var dataSource = null;
			dataSource = new kendo.data.DataSource({
				transport: {
					read: {
						type: "POST",
						url: "/api/V3/Projection/GetProjectionByCriteria",
						contentType: 'application/json',
						dataType: 'json',
						data: criteria
					},
					parameterMap: function (data) {
						return JSON.stringify(data)
					}
				},
				sort: {
					//this equates to CIPLocation currently...
					//field: textField,
					field: "DisplayName",
					dir: "asc"
				}
			});
			dataSource.fetch(function () {
				//console.log("Total Items: " + dataSource.total());
				
				if(dataSource.total() > 0){
					list.setDataSource(dataSource);
					if(listType != "kendoMultiSelect"){
						list.select(-1);
						list.text("Make a selection")
					};
					list.refresh();

					list.enable();
				} else {

					list.setDataSource(dataSource);
					if(listType != "kendoMultiSelect"){
						list.select(-1);
						list.text("Make a selection")
					};
					list.refresh();
					list.enable(false);
				}
			});
		}

	}
};
//end

function initEnum (item) {
	var name = $(item).attr("name");
	var enumId = $(item).data("id");
	
	//console.log("initEnum: " + name);
	
	//creates the kendo list item
	$("input[name=" + name +"]").kendoComboBox({
		autoBind: false,
		dataTextField: "Text",
		dataValueField: "Id",
		dataSource: {
			transport: {
				read: {

					url: "/api/V3/Enum/GetFlatList/" + enumId + "?itemFilter=&includeParents=true",
					data: "",
					type: "GET",
					dataType: "json",
					contentType: 'application/json; charset=UTF-8'








				}
			},
			sort: {
				field: "Text",
				dir: "asc"





			}
		},
		index: 0,
		change: onChange,

	});
	
	function onChange (e) {
		var dataItem = this.dataItem(e.item);
		
		if(newModel[name]) {
			newModel[name] = dataItem;
		}
		
		//check the global change array to see if this change has already been tracked, if not then track it
		if(changes.indexOf(name) < 0){
			changes.push(name);














		}
	}

}

function initRelationship (item) {
	var name = $(item).attr("name");
	var classId = $(item).data("class");
	var projectionId = $(item).data("projection");
	
	var criteria = {
		"Id": projectionId,
		"Criteria": {"Base": {"Expression": {"SimpleExpression": {




			"ValueExpressionLeft": {"GenericProperty": "Id"},


			"Operator": "Like",
			"ValueExpressionRight": {"Value": "%%"}
	}}}}}
			
			//creates the kendo list item
	$("input[name=" + name +"]").kendoComboBox({
		autoBind: false,
		autoClose: false,
		dataTextField: "DisplayName",
		dataValueField: "BaseId",
		dataSource: {
			transport: {
				read: {
					type: "POST",
					url: "/api/V3/Projection/GetProjectionByCriteria",
					contentType: 'application/json',
					dataType: 'json',
					data: criteria
				},
				parameterMap: function (data) {
					return JSON.stringify(data)



				}
			},
			sort: {
				field: "DisplayName",
				dir: "asc"
			}
		},








































		index: -1,
		optionLabel: {
			DisplayName: "Select a related item...",
			BaseId: ""
		},
		change: onChange,
		dataBound: onChange


	});

	
	function onChange (e) {
		var dataItem = this.dataItem(e.item);
		var index = e.sender.selectedIndex;
		
		//check to see if this relationship exists in the model already and updates it

		//console.log("Updating: " + name, "\nSelected Index:" + e.sender.selectedIndex);
		if (index != -1) {
			newModel[name] = dataItem;
		} else { //this is a catch if neither of the above works for some reason
			//console.log("Error: No item selected.");
		}
		
		//check the global change array to see if this change has already been tracked, if not then track it
		if(changes.indexOf(name) == -1){
			changes.push(name);
		}
		
		//update any child relationship lists
		//console.log("Updating children...");
		updateChildRelationships(name);
	}
}




function initChildRelationship (item) {
	var name = $(item).attr("name");
	
	//creates the kendo list item
	$("input[name=" + name +"]").kendoComboBox({
		autoBind: false,
		autoClose: false,
		enable: false,
		dataTextField: "DisplayName",
		dataValueField: "BaseId",
		index: -1,
		optionLabel: {
			DisplayName: "Select a related item...",
			BaseId: ""
		},
		change: function (e) {
			//console.log("ChildRelationship Change: " + name);
			var dataItem = this.dataItem(e.item);
			//check to see if this relationship exists in the model already and updates it
			if (newModel) { //if it doesn't exist yet (new item) then we create it on the model
				newModel[name] = dataItem;
			} else { //this is a catch if neither of the above works for some reason
				//console.log("Error: Model does not exist.");
			}
			
			//check the global change array to see if this change has already been tracked, if not then track it
			if(changes.indexOf(name) < 0){
				changes.push(name);
			}
		}

	});
}

//set the children relationships that relate to this object
function updateChildRelationships (parentName) {
	$("input[data-parent=" + parentName + "]").each(function () {
		var name = $(this).attr("name");
		
		//console.log("Child: " + name);
		





		var property = $(this).data("parent-property");
		var parentList = $("#" + parentName).data('kendoComboBox')
		var parentItem = parentList.dataItem();
		var parentIndex = parentList.selectedIndex;




		if(parentIndex >= 0 && typeof parentItem[property] != "undefined"){
			var dataSource = new kendo.data.DataSource({
				data: parentItem[property],
				sort: {
					field: "DisplayName",
					dir: "asc"
				}
			});
			dataSource.read();

			//creates the kendo list item
			var list = $("input[name=" + name +"]").data('kendoComboBox');
			if(dataSource.total() > 0){
				//console.log("Child Items:" + dataSource.total());
				list.setDataSource(dataSource);
				list.refresh();
				list.select(-1);
				list.enable(true);

			};
		} else {
			//console.log("Child: Does not exist or no realted items on parent.");
			var list = $("input[name=" + name +"]").data('kendoComboBox');
			list.select(-1);
			list.enable(false);	
		};		
	});
}


function initModel (form) {
	var formModel = {};
	
	formModel['ClassTypeId'] = $('#page-setup').data('class');
	$(form).find('input').each(function () {
		var name = $(this).attr('name');
		var type = $(this).attr('type');
		
		if(type === 'enum') {
			formModel[name] = {
				Id: null,
				Name: null,
				Type: "enum"
			}
		} else if (type === 'relationship' || type === 'childrelationship') {
			formModel[name] = {
				BaseId: null,
				DisplayName: null,
				ClassId: $(this).data('class'),
				Type: "relationship"
			}
		} else if (name != 'Submit') {
			formModel[name] = null;
		}
	});
	
	$(form).find('textarea').each(function () {
		var name = $(this).attr('name');
		var type = $(this).attr('type');
		
		formModel[name] = null;
	});
	
	return formModel;
}

function initChangeRequestForm () {
	var name = $("#ciplocation").attr("name");
	var projectionId = $("#ciplocation").data("projection");
	
	var criteria = {
		"Id": projectionId,
		"Criteria": {
			"Base": {
				"Expression": {
					"SimpleExpression": {
						"ValueExpressionLeft": {
							"GenericProperty": "Id"
						},
						"Operator": "Like",
						"ValueExpressionRight": {
							"Value": "%%"
						}
					}
				}
			}
		}
	}
	var stringdata = JSON.stringify(criteria);
	$.ajax({
		url: "/api/V3/Projection/GetProjectionByCriteria",
		data: stringdata,
		type: "POST",
		dataType: "json",
		contentType: 'application/json; charset=UTF-8',
		success: function (data) {
			//function ties the changes to the datalist to the new object model
			function change (e) {
				var dataItem = this.dataItem(e.item);
				//check to see if this relationship exists in the model already and updates it
				if (newModel) {
					newModel[name] = dataItem;
				} else { //this is a catch if neither of the above works for some reason
					console.log("Error: Model does not exist.");
				}
				
				//check the global change array to see if this change has already been tracked, if not then track it
				if(changes.indexOf(name) < 0){
					changes.push(name);
				}
				
				var criteria = {
					"Id": "fb33ac1b-8b90-2282-4d79-6d68e22dc33c",
					"Criteria": {
						"Base": {
							"Expression": {
								"SimpleExpression": {
									"ValueExpressionLeft": {
										"Property": "$Context/Path[Relationship='dd3f02f8-a13b-b5bc-0a89-34b287e36e0c' SeedRole='Source' TypeConstraint='d365f58b-9da3-34e7-d9b0-6268d4f9e9f4']/Property[Type='d365f58b-9da3-34e7-d9b0-6268d4f9e9f4']/6b8fd9d3-a5dd-d333-2449-9940507d76ae$"
									},
									"Operator": "Equal",
									"ValueExpressionRight": {
										"Value": dataItem.CIPLocation
									}
								}
							}
						}
					}
				};
				var stringdata = JSON.stringify(criteria);
				$.ajax({
					url: "/api/V3/Projection/GetProjectionByCriteria",
					data: stringdata,
					type: "POST",
					dataType: "json",
					contentType: 'application/json; charset=UTF-8',
					success: function (data) {
						
						var dataSource = new kendo.data.DataSource({
							data: data,
							sort: {
								field: "DisplayName",
								dir: "asc"
							}
						});
						var assetSelect = $("#asset").data("kendoMultiSelect");
						assetSelect.setDataSource(dataSource);
						assetSelect.refresh();
						assetSelect.enable(true);
					}
				});
			}
			
			//creates the kendo list item
			$("#ciplocation").kendoComboBox({
				dataTextField: "DisplayName",
				dataValueField: "BaseId",
				dataSource: {
					data: data,
					sort: {
						field: "DisplayName",
						dir: "asc"
					}
				},
				index: -1,
				optionLabel: {
					DisplayName: "Select a related item...",
					BaseId: ""
				},
				change: change
			});
			var cipSelect = $("#ciplocation").data("kendoComboBox");
			console.log("Setup CR CIP Location dropdown.");
			
			$("#asset").kendoMultiSelect({
				placeholder: "Select assets...",
				template: '<span class="order-id">#= data.DisplayName #</span>',
				dataTextField: "DisplayName",
				dataValueField: "BaseId",
				height: 520,
				autoBind: false,
				autoClose: false,
				enabled: false,
				change: function (e) {
					
					//check to see if this relationship exists in the model already and updates it
					if (newModel) { //if it doesn't exist yet (new item) then we create it on the model
						newModel["RelatesToConfigItem"] = this.dataItems();
					} else { //this is a catch if neither of the above works for some reason
						console.log("Error: Model does not exist.");
					}
					
					//check the global change array to see if this change has already been tracked, if not then track it
					if(changes.indexOf(name) < 0){
						changes.push(name);
					}
				}
			});
		}
	})
}

function createCR (model) {


	app.lib.mask.apply('Creating Request...');
	//Logged in User Id
	var uid = session.user.Id;
	var udn = session.user.Name;
	//var divisionGroup = "Generation"; //or "T&D"
	//var MCDCI_status = "Active"; //or "Retired"
	
	//gets the current type of the sr request (edit/create)
	var type = pages[selectedPage].currentType;
	
	//oakwood added to set division based on page
	var divisionpage = pages[selectedPage].currentPage;
	//Change Request Template ID for the default template we want to use
	//Find template ID with powershell smlets Get-SCSMObjectTemplate | ?{$_.Displayname -like 'KCPL Generation-TD*'} | fl

	//CorpSec Change Template --KCPL DEV GUID c477fa6b-2c20-cbe1-f595-d64eaa557ed5 ,KCPL QA GUID f448a6eb-cd92-3815-eb97-0550ed3f0a83

	var templateId = 'c477fa6b-2c20-cbe1-f595-d64eaa557ed5' 

	
	//Serialize the form to use in automation
	var description = "Related Assets to be Changed:\n";

	description += "Selected Location: " + ($("#ci-location-select").data("kendoComboBox").dataItem()).DisplayName + "\n";
	description += "Related Assets:\n"
	$("#ci-asset-select").data("kendoMultiSelect").dataItems().forEach(function (entry) {
			
			if(entry.AssetName != null){
			
			description += "\t" + entry.DisplayName + "    " + entry.AssetName +"\n";
			
			}else{
				
			description += "\t" + entry.DisplayName + "\n";	
				
			}
			
			
			
		});
		
	//console.log(description);
	
	$.ajax({
		url: "/api/V3/Projection/CreateProjectionByTemplate",
		data: {id: templateId, createdById: uid},
		type: "GET",
		success: function (data) {
			

			//Create the new CR, filling in the required fields

			//Cliff Changed for New Requirement 09092015//data.Title = model.Title;
				//test
	
				//if (divisionpage === "kcpl_CR_gen_ci.html"){
					
				//	data.Title = "MCDCI Generation Asset Change Request - " + ($("#ciplocation").data("kendoComboBox").dataItem()).DisplayName;
					
				//} else {
					
				//	data.Title = "MCDCI Asset Change Request";
					
				//}
				
	
	
	
	
	
				//description = division;
	
				//test end
			
			
			
			
			data.Title = "MCDCI Asset Change Request - " + ($("#ci-location-select").data("kendoComboBox").dataItem()).DisplayName;
			data.DisplayName = data.Id + ": " + data.Title;
			data.Description = description;
			//set values selected in form
			data.Reason = model.Reason;
			data.Area = model.Area;
			data.Risk = model.Risk;
			data.Impact = model.Impact;
			data.Priority = model.Priority;
			data.RiskAssessment = model.RiskAssessment;
			data.TestPlan = model.TestPlan;
			data.ImplementationPlan = model.ImplementationPlan;
			data.Notes = model.Notes;
			data.BackOutPlan = model.BackOutPlan;

	



			data.RequestedWorkItem = [{
				ClassTypeId: "fb882d6b-bf4c-b78a-e1fe-f761114bc030",
				BaseId: uid,
				DisplayName: udn
			}];
			data.CreatedWorkItem = [{
				ClassTypeId: "fb882d6b-bf4c-b78a-e1fe-f761114bc030",
				BaseId: uid,
				DisplayName: udn
			}];
			data.NameRelationship = [{
				Name: "RequestedWorkItem",
				RelationshipId: "DFF9BE66-38B0-B6D6-6144-A412A3EBD4CE"
			},
			{
				Name: "RelatesToWorkItem",
				RelationshipId: "cb6ce813-ea8d-094d-ee5a-b755701f4547"
			},
			{
				Name: "CreatedWorkItem",
				RelationshipId: "df738111-c7a2-b450-5872-c5f3b927481a"
			},
			{
				Name: "RelatesToConfigItem",
				RelationshipId: "d96c8b59-8554-6e77-0aa7-f51448868b43"
			},
			{
				Name: "HasRelatedWorkItems",
				RelationshipId: "b73a6094-c64c-b0ff-9706-1822df5c2e82"
			}];
			data.RelatesToConfigItem = model.RelatesToConfigItem;
			
			var strData = { "formJson":{"current": data }}
			
			$.ajax({
				url: "/api/V3/Projection/Commit",
				type: "POST",
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				data: JSON.stringify(strData) ,
				success: function (d) {
					//console.log(data.Id + " created successfully.");
					window.location.href = window.location.protocol + "//" + window.location.hostname + "/ChangeRequest/Edit/" + data.Id
				}
			});
			
		}
	});
}
