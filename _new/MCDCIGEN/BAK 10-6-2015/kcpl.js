var ciModel;
var newModel;
var changes = [];
var pages = [
	{


		//supported types are Edit, Create, and CR
		currentPage: "kcpl_create_gen_ci.html",
		currentType: "Create",
		currentTitle: "MCDCI GEN CI Create Request"
	},
	{
		currentPage: "kcpl_edit_gen_ci.html",
		currentType: "Edit",
		currentTitle: "MCDCI GEN CI Edit Request"
	},
	{
		currentPage: "kcpl_retire_gen_ci.html",
		currentType: "Edit",
		currentTitle: "MCDCI GEN CI Retire Request"
	},
	{
		currentPage: "kcpl_CR_gen_ci.html",
		currentType: "CR",
		currentTitle: "MCDCI GEN CI Change Request"
	},
	{
		currentPage: "kcpl_grid_gen_ci.html",
		currentType: "Grid",
		currentTitle: "MCDCI Grid"
	}
	
];
var selectedPage = 0;
var validator = null;
var formStatus = null;

$('button[type="reset"]').on("click",function(){
	clearForm();
});

//onReady function that loads each run
$(function () {
	//programatically add a new button for the grid
	$('#custom-form-nav .btn-group').append('<button type="button" class="btn btn-default" data-id="4">Asset Table</button>');
	//$('#custom-form-nav .btn-group').append('<button type="button" class="btn btn-default" data-id="5">Asset Table SSRS</button>');

	var buttons = $("#custom-form-nav div button");
	
	buttons.on('click', function() {
		app.lib.mask.apply('Loading...'); //apply the page mask then the button is clicked.
		$(this).siblings("button").removeClass("active");
		$(this).addClass("active");
		var page = $(this).data("id");
		loadPage(pages[page]);
		selectedPage = page;		
	});
     
     var modalWidth = $('#confirm').width();
     $('#confirm').css("left", "50%");
     $('#confirm').css("width", modalWidth);
     $('#confirm').css("margin", (modalWidth/2)*-1);
});

function loadPage(page) {
	var currentPage = page.currentPage;
	var currentType = page.currentType;
	
	$('#custom-form-area').load('/CustomSpace/MCDCIGEN/' + currentPage, function(e){
		//add selection list to the top
		if(currentType === 'Edit'){
			//initialize the form fields
			initForm(currentType);

			loadList('#ci-location-select', "CIPLocation", "BaseId", true);
			loadList(
				'#ci-asset-select', 
				"AssetName", 
				"BaseId",
				false,
				"kendoComboBox",
				'<span class="k-state-default"><table><tbody><tr>' +
				'<td>#: data.AssetName #</td>' +
				'<td>#: data.AssetUniqueID #</td>' +
				'<td>#if(data.Target_03923330_e879_4926_8523_099ce3b6e1ed){##: data.Target_03923330_e879_4926_8523_099ce3b6e1ed.DisplayName##}else{#Empty#}#</td>' +
				'</tr></tbody></table></span>'
			)

		}
		//if it's a create form then we don't have an existing model so we create our own 
		else if(currentType === 'Create') {
			//initialize the form fields
			var form = $('#ci-update-form');
			newModel = initModel(form);
			initForm(currentType);
		}
		else if(currentType === 'CR') {
			var form = $("#ci-update-form");
			newModel = initModel(form);
			initForm(currentType);
			loadList('#ci-location-select', "CIPLocation", "BaseId", true);
			loadList(
				'#ci-asset-select', 
				"AssetName", 
				"BaseId",
				false,
				"kendoMultiSelect",
				'<span class="k-state-default"><table><tbody><tr>' +
				'<td>#: data.AssetName #</td>' +
				'<td>#: data.AssetUniqueID #</td>' +
				'<td>#if(data.Target_03923330_e879_4926_8523_099ce3b6e1ed){##: data.Target_03923330_e879_4926_8523_099ce3b6e1ed.DisplayName##}else{#Empty#}#</td>' +
				'</tr></tbody></table></span>'
			)
		}



		app.lib.mask.remove(); //remove the page mask after the form is loaded
		//end
	});
}

//clears the form when no result is found (empty or invalid CI name)
function clearForm() {
	$('#ci-update-form input').each(function (){
		var inputName = $(this).attr("name");
		var type = $(this).attr("type");

		//check if input is an enum and init if so
		if(type === "enum") {
			var dropList = $('input[name='+ inputName +']').data("kendoComboBox");
			dropList.select(0);
		} else if(type === "relationship") {
			var dropList = $('input[name='+ inputName +']').data("kendoComboBox");
			dropList.select(0);
		} else {
			if(inputName != "Submit") {
				$('input[name='+ inputName +']').val("");

				$('textarea[name='+ inputName +']').val("");
			}
		}
	});
}

//initializes the form, adding form controls and change events
function initForm(currentType) {
	//console.log("Current Type:" + currentType);
	if(currentType){
		//initialize the form fields
		$('#ci-update-form input[type=relationship]').each(function (){
			var inputName = $(this).attr("name");
			initRelationship($('input[name=' + inputName + ']'));
		});
		$('#ci-update-form input[type=childrelationship]').each(function (){
			var inputName = $(this).attr("name");
			initChildRelationship($('input[name='+ inputName +']'));
		});
		$('#ci-update-form input[type=enum]').each(function (){
			var inputName = $(this).attr("name");
			initEnum($('input[name=' + inputName + ']'));
		});
		$('#ci-update-form input[type=text]').each(function (){
			var inputName = $(this).attr("name");
			$('input[name='+ inputName +']').on("input", function(){ 
					newModel[inputName] = $(this).val(); 
					var index = changes.indexOf(inputName);
					if(index < 0){
						changes.push(inputName);
					}
				});
		});
		$('#ci-update-form textarea[type=text]').each(function (){
			var inputName = $(this).attr("name");
			$('textarea[name='+ inputName +']').on("input", function(){ 
					newModel[inputName] = $(this).val(); 
					var index = changes.indexOf(inputName);
					if(index < 0){
						changes.push(inputName);
					}
				});
		});
	} 

	
	//console.log("Setting up form validation.")
	//add form validation
	validator = $('#ci-update-form').kendoValidator({
			rules: {
				hasItems: function (input) {
					if(input.is("#asset")){
						//Get multiselect instance
						//console.log("Input is \"RelatesToConfigItem\".");
						var ms = input.data("kendoMultiSelect");
						if(ms.value().length === 0){
							return false;
						}
					}
					return true;
				}
			},
			messages: {
				hasItems: "Please select at least one asset."
			}
		}).data("kendoValidator");
	formStatus = $(".status");

	$("form").submit(function(event) {
		event.preventDefault();

		if(currentType == "Create"){
			newModel['MCDCI_Status'] = {
				Id: $('#MCDCI_Status').val(),
				Text: "Active"
			};
			newModel['Division_Group'] = {
				Id: $('#Division_Group').val(),
				Text: "Generation"

			};


								
			newModel['AssetUniqueID'] = $('#AssetUniqueID').val();
		}
		//console.log("Submit button pressed, checking validation.");
		if(validator.validate()) {
			if(currentType === "CR"){
				formStatus.text("Info: Your request has been submitted!");
				//console.log("Creating the CR.");
				createCR(newModel);
			}
			else {
				formStatus.text("Info: Your request has been submitted!");
				//console.log("Creating the SR.");
				createSR(newModel);
			}
		} else {
			formStatus.text("Oops! There is invalid data in the form.")
				.removeClass("valid")
				.addClass("invalid");
			//console.log("Invalid fields.");
		}
		
	});
}
//end


//fills the form fields on selection of an asset
function fillForm(model) {
	$('#ci-update-form input').each(function (){
		var inputName = $(this).attr("name");
		var type = $(this).attr("type");
		
		if(type === "enum"){
			var dropList = $('input[name='+ inputName +']').data("kendoComboBox");
			if(model[inputName]) {
				dropList.value(model[inputName].Id);
			} else {
				dropList.select(-1);
			}
		} else if (type === "relationship") {
			var dropList = $('input[name='+ inputName +']').data("kendoComboBox");
			if(model[inputName]) {
				dropList.value(model[inputName].BaseId);
			} else {
				dropList.select(-1);
			}
		} else {
			$('input[name='+ inputName +']').val(model[inputName]);
		}
	});
	$('#ci-update-form textarea').each(function () {
		var inputName = $(this).attr("name");
		var type = $(this).attr("type");
		
		$('textarea[name=' + inputName + ']').val(model[inputName]);
	});
}
//end

/* SR Creation Area
Here we perform all the actions required on submit to build the SR and submit it
with the changed data so it can be approved
*/
function buildDescription(cList, oModel, nModel, type) {
                var result = "";
	if($.type(nModel["AssetName"] != "undefined")) {
		result = type + " CI Request Specifics: " + nModel["AssetName"] + " " + nModel["AssetUniqueID"]+ "\n";
	} 
                for(var i=0; i < cList.length;i++) {
                                if($.type(oModel) != "undefined" && $.type(oModel[cList[i]]) != "undefined") {
                                                
												if (typeof nModel[cList[i]] == "object" && typeof nModel[cList[i]].ClassName != "undefined"){
                                                                result += nModel[cList[i]].ClassName + " 1a: " + oModel[cList[i]].DisplayName + " > " + nModel[cList[i]].DisplayName + "\n";
                                                } else if (typeof nModel[cList[i]] == "object" && typeof nModel[cList[i]].Text != "undefined") {
                                                                result += cList[i] + nModel[cList[i]] + "2: " + oModel[cList[i]].Text + " > " + nModel[cList[i]].Text + "\n";
                                                } else {
                                                                result += cList[i] + " 3: " + oModel[cList[i]] + " > " + nModel[cList[i]] + "\n";
                                                }
                                } else {
                                                if(typeof nModel[cList[i]] == "object" && typeof nModel[cList[i]].ClassName != "undefined"){
                                                                result += " 3b " + nModel[cList[i]].ClassName + " : " + oModel[cList[i]] + " > " + nModel[cList[i]].DisplayName + "\n";
                                                } else if(typeof nModel[cList[i]] == "object" && typeof nModel[cList[i]].Text != "undefined") {
                                                                result += cList[i] + "4: " + nModel[cList[i]].Text + "\n";
                                                } else {
                                                                result += cList[i] + "5: " + nModel[cList[i]] + "\n";
                                                }
                                }
                }
                return result;
}


function createSR (model) {

	app.lib.mask.apply('Creating Service Request...');
	//Logged in User Id
	var uid = session.user.Id;
	var udn = session.user.Name;
	
	//gets the current type of the sr request (edit/create)
	var type = pages[selectedPage].currentType;
	var title = pages[selectedPage].currentTitle;
	
	//Service Request Template ID for the default template we want to use
	//var templateId = '03bc9162-041f-c987-8ce4-a5547cd9ca04'; //Default Service Request
	var templateId = '64156c0d-c053-3b4a-912a-49fa673f56fc'; //Generation Asset Modify SR Template *UPDATE ON INSTALL*
	
	//Serialize the form to use in automation
	var description = buildDescription(changes, ciModel, model, type);
	//console.log(type, description);
	
	$.ajax({
		url: "/api/V3/Projection/CreateProjectionByTemplate",
		data: {id: templateId, createdById: uid},
		type: "GET",
		success: function (data) {
			//console.log(data);

			//Create the new SR, filling in the required fields
			data.Title = title;
			data.DisplayName = data.Id + ": " + data.Title;
			data.Description = description;
			data.RequestedWorkItem = [{
				ClassTypeId: "fb882d6b-bf4c-b78a-e1fe-f761114bc030",
				BaseId: uid,
				DisplayName: udn
			}];
			data.CreatedWorkItem = [{
				ClassTypeId: "fb882d6b-bf4c-b78a-e1fe-f761114bc030",
				BaseId: uid,
				DisplayName: udn
			}];
			
			//relates the configuration item if this is a edit request and the Ci already exists
			if(model.ClassId) {
				data.RelatesToConfigItem = [{
					ClassTypeId: model.ClassId,
					BaseId: model.BaseId,
					Id: model.BaseId
				}];
			}
			
			data.NameRelationship = [{
				Name: "RequestedWorkItem",
				RelationshipId: "DFF9BE66-38B0-B6D6-6144-A412A3EBD4CE"
			},
			{
				Name: "RelatesToWorkItem",
				RelationshipId: "cb6ce813-ea8d-094d-ee5a-b755701f4547"
			},
			{
				Name: "CreatedWorkItem",
				RelationshipId: "df738111-c7a2-b450-5872-c5f3b927481a"
			},
			{
				Name: "RelatesToConfigItem",
				RelationshipId: "d96c8b59-8554-6e77-0aa7-f51448868b43"
			}];
			data.Priority = {
				Id: "1e070214-693f-4a19-82bb-b88ee6362d98",
				Name: "Low",
				uid: "6f6e89f2-a506-43fb-9b2d-5cdcd44a84be"
			};   
			data.Urgency = {
				Id: "b02d9277-a9fe-86f1-e95e-0ba8cd4fd075",
				Name: "Low",
				uid: "3f3dcbe9-23aa-4f2b-8246-905faf287db6"  
			};
			//extension properties here specific to this utilization
			newModel["ProjectionId"] = $('#page-setup').data('projection-full');
			data.ext_ci_request_json = JSON.stringify(newModel);
			data.ext_ci_request_type = type;
			
			var strData = { "formJson":{"current": data }}
			
			$.ajax({
				url: "/api/V3/Projection/Commit",
				type: "POST",
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				data: JSON.stringify(strData) ,
				success: function (d) {
					//console.log(data.Id + " created successfully.");
					window.location.href = window.location.protocol + "//" + window.location.hostname + "/ServiceRequest/Edit/" + data.Id
				}
			});
			
		}
	});


}
//end