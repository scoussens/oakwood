{
    "Id": "fb33ac1b-8b90-2282-4d79-6d68e22dc33c",
    "Criteria": {
		"Base": {
			"Expression": {
				"And": {
					"Expression": [{
						"SimpleExpression": {
							"ValueExpressionLeft": {
								"GenericProperty": "Id"
							},
							"Operator": "Like",
							"ValueExpressionRight": {
								"Value": "%%"
							}
						}
					},
					{
						"SimpleExpression": {
							"ValueExpressionLeft": {
								"Property": "$Context/Property[Type='d3901417-bd0e-318a-682b-af72a0dd24db']/9c775305-ee05-389b-df5a-68f3d79c7d41$"
							},
							"Operator": "Equal",
							"ValueExpressionRight": {
								"Value": "e09a3a6b-4386-e818-5a96-4291ee03f895"
							}
						}
					}]
				}
			}
		}
	}
}


{
	"Id": "1edc9b89-aa11-194f-23e5-9e6573726f01",
	"Criteria": {
		"Base": {
			"Expression": {
				"And": {
					"Expression": [{
						"SimpleExpression": {
							"ValueExpressionLeft": {
								"GenericProperty": "DisplayName"              
							},
							"Operator": "Like",
							"ValueExpressionRight": {
								"Value": "%Phone%"              
							}            
						}          
					},
					{
						"SimpleExpression": {
							"ValueExpressionLeft": {
								"GenericProperty": "DisplayName"              
							},
							"Operator": "Like",
							"ValueExpressionRight": {
								"Value": "%Smart%"              
							}            
						}          
					}]         
				}  
			}
		}
	}
}
