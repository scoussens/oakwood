TypeProj
MCDCI_ESP_Form_TypeProjection                               4160f295-c642-bef5-3edb-747b021850e3
MCDCI_CIPLocation_Form_TypeProjection                       1fa6e00d-b24c-ba5a-3473-4dfc307b8c8a
MCDCI_BESCyberSystem_Form_TypeProjection                    6a224b7e-cba2-1d54-da26-2ed740c10f0b
MCDCI_Asset_Form_TypeProjection                             2bd8b1e5-c511-32c0-2172-5a6d2b0f5b13
MCDCI_PSP_Form_TypeProjection                               c0284df1-03d1-9ac0-fa54-a8dfc68627a8
MCDCI_OperatingSystem.Minimum.Projection                    cba8e83b-f012-45d0-8d23-2e9b25904fc3
MCDCI_ESP.Minimum.Projection                                b56953ab-e3a6-9df2-96cc-010161b686e3
MCDCI_ESP.Full.Projection                                   8e0b7548-d221-6212-0a50-684606101581
MCDCI_DesignatedSystem.Minimum.Projection                   a9203661-cbf1-636d-35ac-d8ca456102c6
MCDCI_PhysicalHost.Minimum.Projection                       4b7f2378-451b-b9c9-1be9-5cc56fe9c1ef
MCDCI_TypeOfDevice.Minimum.Projection                       6b37b297-a759-d831-d1cd-ba9bc832b1e5
MCDCI_CIPLocation.Full.Projection                           78b60de3-dacd-0d5f-58ac-cff49a04ad2d
MCDCI_CIPLocation.Minimum.Projection                        9846ff5f-eb09-6287-246d-d07e694a6f5e
MCDCI_PhysicalLocationDetails.Minimum.Projection            92288198-919f-e26e-6c18-d999307d5475
MCDCI_BESCyberSystem.Minimum.Projection                     aa26e6af-bb22-4669-d86c-90da2ea154af
MCDCI_BESCyberSystem.Full.Projection                        248f7de7-413f-3f17-1dcc-3a20e0e71654
MCDCI_RecoveryProcedure.Minimum.Projection                  71c6bba4-8994-15f1-f34d-082a962dc59f
MCDCI_Asset.Minimum.Projection                              edadde04-c025-098b-bcba-599d9865f1be
MCDCI_Asset.Full.Projection                                 fb33ac1b-8b90-2282-4d79-6d68e22dc33c
MCDCI_Asset.CiresonCustomGenTDAssetDropDown.Projection	    e024acad-312d-bd73-d636-6220ff482057
MCDCI_Manufacturer.Minimum.Projection                       020c7949-4362-6c0a-89b9-ca8b87b7bcf9
MCDCI_PSP.Minimum.Projection                                a5db89ba-a084-fb47-d067-e144cb9e4085
MCDCI_PSP.Full.Projection                                   ac08e9a3-a940-8e4c-cb9a-88fde1f01129
MCDCI_RecoveryPlan.Minimum.Projection                       27c9560b-9ed1-fc87-78ea-bcbec0c9a8f8
MCDCI_DeviceClass.Minimum.Projection                        4b9b13eb-d30b-fd44-0c74-16846c4597df

Name                                                        Id
----                                                        --
MCDCI_ESP_Form_TypeProjection                               4160f295-c642-bef5-3edb-747b021850e3
MCDCI_CIPLocation_Form_TypeProjection                       1fa6e00d-b24c-ba5a-3473-4dfc307b8c8a
MCDCI_BESCyberSystem_Form_TypeProjection                    6a224b7e-cba2-1d54-da26-2ed740c10f0b
MCDCI_Asset_Form_TypeProjection                             2bd8b1e5-c511-32c0-2172-5a6d2b0f5b13
MCDCI_PSP_Form_TypeProjection                               c0284df1-03d1-9ac0-fa54-a8dfc68627a8
MCDCI_OperatingSystem.Minimum.Projection                    cba8e83b-f012-45d0-8d23-2e9b25904fc3
MCDCI_ESP.Minimum.Projection                                b56953ab-e3a6-9df2-96cc-010161b686e3
MCDCI_ESP.Full.Projection                                   8e0b7548-d221-6212-0a50-684606101581
MCDCI_DesignatedSystem.Minimum.Projection                   a9203661-cbf1-636d-35ac-d8ca456102c6
MCDCI_PhysicalHost.Minimum.Projection                       4b7f2378-451b-b9c9-1be9-5cc56fe9c1ef
MCDCI_TypeOfDevice.Minimum.Projection                       6b37b297-a759-d831-d1cd-ba9bc832b1e5
MCDCI_Asset.CiresonCustomGenTDAssetDropDown.Projection      e024acad-312d-bd73-d636-6220ff482057
MCDCI_CIPLocation.Full.Projection                           78b60de3-dacd-0d5f-58ac-cff49a04ad2d
MCDCI_CIPLocation.Minimum.Projection                        9846ff5f-eb09-6287-246d-d07e694a6f5e
MCDCI_PhysicalLocationDetails.Minimum.Projection            92288198-919f-e26e-6c18-d999307d5475
MCDCI_BESCyberSystem.Minimum.Projection                     aa26e6af-bb22-4669-d86c-90da2ea154af
MCDCI_BESCyberSystem.Full.Projection                        248f7de7-413f-3f17-1dcc-3a20e0e71654
MCDCI_RecoveryProcedure.Minimum.Projection                  71c6bba4-8994-15f1-f34d-082a962dc59f
MCDCI_Asset.Minimum.Projection                              edadde04-c025-098b-bcba-599d9865f1be
MCDCI_Asset.Full.Projection                                 fb33ac1b-8b90-2282-4d79-6d68e22dc33c
MCDCI_Manufacturer.Minimum.Projection                       020c7949-4362-6c0a-89b9-ca8b87b7bcf9
MCDCI_PSP.Minimum.Projection                                a5db89ba-a084-fb47-d067-e144cb9e4085
MCDCI_PSP.Full.Projection                                   ac08e9a3-a940-8e4c-cb9a-88fde1f01129
MCDCI_RecoveryPlan.Minimum.Projection                       27c9560b-9ed1-fc87-78ea-bcbec0c9a8f8
MCDCI_DeviceClass.Minimum.Projection                        4b9b13eb-d30b-fd44-0c74-16846c4597df






Source
Rel_AssetToBESCyberSystem                                   Source_991cc27a_0a63_4416_9051_fb552d55ed01
Rel_AssetToCIPLocation                                      Source_071588da_8f68_4335_bebe_2cd4a5c458ae
Rel_AssetToDesignatedSystem                                 Source_96a5ebe4_a3f9_4710_a645_63fafa26ff9f
Rel_AssetToDeviceClass                                      Source_f5f82257_3cc8_42bf_b09c_59e00801e913
Rel_AssetToESP                                              Source_970c69d8_d9d7_4963_af1a_991a9df6c7ce
Rel_AssetToManufacturer                                     Source_3959a5e7_9590_4391_9df2_2bff5b450c3f
Rel_AssetToOperatingSystem                                  Source_c51f8a22_fc85_4d02_aca7_a772e32baa15
Rel_AssetToPhyscialHost                                     Source_2dc3217f_2a6a_44e1_b5db_99adaeed705f
Rel_AssetToPhysicalLocationDetails                          Source_54127ebb_b006_4fc8_9f36_8db0abbc68a3
Rel_AssetToPSP                                              Source_682eafb3_c71e_4697_8b7d_20b256385c7d
Rel_AssetToRecoveryPlan                                     Source_295cc2c0_5023_4857_9064_26a54e545835
Rel_AssetToRecoveryProcedure                                Source_15d49572_10d3_46f6_b41c_c0a8f44d155c
Rel_AssetToTypeOfDevice                                     Source_64930c32_f860_4597_bf9c_8742c47aaa5f
Rel_BESCycberSystemToCIPLocation                            Source_0a99f8f4_007f_4121_a626_0b44717337aa
Rel_BESCycberSystemToRecoveryPlan                           Source_c881ca2d_b13e_4859_bf87_4fd188ee7260
Rel_ESPToCipLocation                                        Source_34f6a691_468a_46fd_81f0_d704e85697a4
Rel_PSPToCIPLocation                                        Source_a5b704dc_84cc_4120_bce3_a17b3a26ab83

Target
Rel_AssetToBESCyberSystem                                   Target_74621fd0_36c6_47be_a679_4bbdbbc759eb
Rel_AssetToCIPLocation                                      Target_085a4b89_8cd4_40e0_8585_4a03b9e37cd5
Rel_AssetToDesignatedSystem                                 Target_e63e3095_af07_41d0_99e4_cf78062d82c8
Rel_AssetToDeviceClass                                      Target_36cd28d2_45a7_4f27_bb30_b9ebbd297909
Rel_AssetToESP                                              Target_ca178cb0_88fc_4210_9977_98a81c40a7ad
Rel_AssetToManufacturer                                     Target_0a035203_a28e_42f5_893d_597124f5cac4
Rel_AssetToOperatingSystem                                  Target_b43e22df_da3e_45d2_aead_f2eec1b68aed
Rel_AssetToPhyscialHost                                     Target_4d8a0c3b_bd5b_4bac_9b1b_50982ec436a1
Rel_AssetToPhysicalLocationDetails                          Target_4f1bb4a3_6cc1_4802_8555_4b7d9cfed299
Rel_AssetToPSP                                              Target_9391e8cd_a05a_42bc_b92d_d9cff0f2d94a
Rel_AssetToRecoveryPlan                                     Target_02e4ea75_fb33_45e7_aa66_b3dda99225b0
Rel_AssetToRecoveryProcedure                                Target_0537cd2f_9ce1_4c4f_ae43_4b1907b421af
Rel_AssetToTypeOfDevice                                     Target_03923330_e879_4926_8523_099ce3b6e1ed
Rel_BESCycberSystemToCIPLocation                            Target_2243c189_dfba_4d6d_b784_4b5f9981fdf5
Rel_BESCycberSystemToRecoveryPlan                           Target_26767152_f53b_4ff9_9fa6_b77b7e041270
Rel_ESPToCipLocation                                        Target_ca7c6d50_2b50_4fd2_9864_3d2042e4d243
Rel_PSPToCIPLocation                                        Target_56047dca_c82a_447d_bd86_66a7b725f5d6

Class
MCDCI_Asset                                             d3901417-bd0e-318a-682b-af72a0dd24db
MCDCI_BESCyberSystem                                    720fea8c-888e-6748-87cd-80fe1d58fbb1
MCDCI_CIPLocation                                       d365f58b-9da3-34e7-d9b0-6268d4f9e9f4
MCDCI_DesignatedSystem                                  4578d050-a0e1-c1ce-82ab-26f866b2640c
MCDCI_DeviceClass                                       d1375ef8-5d41-d47a-d4c9-ee1415fa89ab
MCDCI_ESP                                               da82aae1-8e8a-6214-6a46-15ca42700e4a
MCDCI_Manufacturer                                      f6e2be49-97a4-2121-11c0-ced7d1dfdbee
MCDCI_OperatingSystem                                   aaca415b-bbda-4f6c-e527-0eaa3f524bd4
MCDCI_PhysicalHost                                      5e123e4f-916c-07d1-5e22-335d9673133f
MCDCI_PhysicalLocationDetails                           33cd050b-ac9a-b01e-2c79-7f0e5ac3cf84
MCDCI_PSP                                               0fce4c07-0558-5b7a-5546-d0426ea22b47
MCDCI_RecoveryPlan                                      3324f33e-a1cf-56ab-2d0e-e8f3fcd59a4f
MCDCI_RecoveryProcedure                                 c2998c41-89d1-f476-e717-9b77078dff4f
MCDCI_TypeOfDevice                                     	9602d28b-1445-3a69-15e5-5b644f06ba92

Name                                    DisplayName                             Id
----                                    -----------                             --
MCDCI_Asset                             MCDCI_Asset                             d3901417-bd0e-318a-682b-af72a0dd24db
MCDCI_BESCyberSystem                    MCDCI_BESCyberSystem                    720fea8c-888e-6748-87cd-80fe1d58fbb1
MCDCI_CIPLocation                       MCDCI_CIPLocation                       d365f58b-9da3-34e7-d9b0-6268d4f9e9f4
MCDCI_DesignatedSystem                  MCDCI_DesignatedSystem                  4578d050-a0e1-c1ce-82ab-26f866b2640c
MCDCI_DeviceClass                       MCDCI_DeviceClass                       d1375ef8-5d41-d47a-d4c9-ee1415fa89ab
MCDCI_ESP                               MCDCI_ESP                               da82aae1-8e8a-6214-6a46-15ca42700e4a
MCDCI_Manufacturer                      MCDCI_Manufacturer                      f6e2be49-97a4-2121-11c0-ced7d1dfdbee
MCDCI_OperatingSystem                   MCDCI_OperatingSystem                   aaca415b-bbda-4f6c-e527-0eaa3f524bd4
MCDCI_PhysicalHost                      MCDCI_PhysicalHost                      5e123e4f-916c-07d1-5e22-335d9673133f
MCDCI_PhysicalLocationDetails           MCDCI_PhysicalLocationDetails           33cd050b-ac9a-b01e-2c79-7f0e5ac3cf84
MCDCI_PSP                               MCDCI_PSP                               0fce4c07-0558-5b7a-5546-d0426ea22b47
MCDCI_RecoveryPlan                      MCDCI_RecoveryPlan                      3324f33e-a1cf-56ab-2d0e-e8f3fcd59a4f
MCDCI_RecoveryProcedure                 MCDCI_RecoveryProcedure                 c2998c41-89d1-f476-e717-9b77078dff4f
MCDCI_TypeOfDevice                      MCDCI_TypeOfDevice                      9602d28b-1445-3a69-15e5-5b644f06ba92







Base Property ID
Division_Group						9c775305-ee05-389b-df5a-68f3d79c7d41

Enums
MCDCI_ImpactRating                                      f4fc42e9-24f6-4b5c-e16f-0a71f9ed94db
MCDCI_DivisionGroup                                     dbaec815-62c4-ddd2-2e0b-6cac777896ea
MCDCI_AssetStatus                                       5105cfda-2ff9-5965-59cd-c0190ac37886

Enum Property ID
KCPL.MCDCI.DivisionGroup.TD.enum			54fbc7ec-cebe-b2e9-beb2-19399c11fcc8
KCPL.MCDCI.DivisionGroup.CorpSec.enum			6a2dbfa0-9547-e50c-367b-4c22270b0876
KCPL.MCDCI.DivisionGroup.EMSIT.enum			7ad9a846-53af-04df-17bc-5806065d6beb
KCPL.MCDCI.DivisionGroup.Generation.enum		7e454cb8-cfc4-b94a-b3c3-c593d7fe619f

Enum ID Table

Name                                    DisplayName                             Id
----                                    -----------                             --
MCDCI_ImpactRating                      MCDCI Impact Rating                     f4fc42e9-24f6-4b5c-e16f-0a71f9ed94db
KCPL.MCDCI.DivisionGroup.TD.enum        T&D                                     54fbc7ec-cebe-b2e9-beb2-19399c11fcc8
KCPL.MCDCI.ImpactRating.BCAmedium.enum  BCA (Medium)                            5d2c2c1d-cf1a-519e-a57b-36a85e50926e
KCPL.MCDCI.ImpactRating.CCA.enum        CCA                                     7e04dbd1-e9b5-a72f-833b-46380adf6913
KCPL.MCDCI.ImpactRating.BCAlow.enum     BCA (Low)                               b46a0562-b0c0-a8d3-7b61-49ea14937127
KCPL.MCDCI.DivisionGroup.CorpSec.enum   CorpSec                                 6a2dbfa0-9547-e50c-367b-4c22270b0876
KCPL.MCDCI.ImpactRating.EACMS.enum      EACMS                                   39eb1172-fb32-4ab4-2252-514ce7883286
KCPL.MCDCI.DivisionGroup.EMSIT.enum     EMS/IT                                  7ad9a846-53af-04df-17bc-5806065d6beb
KCPL.MCDCI.ImpactRating.NBCA.enum       N-BCA                                   3b290f96-8bc6-521d-d222-5a0ed2346c2e
MCDCI_DivisionGroup                     MCDCI Division/Group                    dbaec815-62c4-ddd2-2e0b-6cac777896ea
KCPL.MCDCI.ImpactRating.PACS.enum       PACS                                    ec4af258-9e7e-3724-76bf-7a23ad532ece
KCPL.MCDCI.AssetStatus.Active.enum      Active                                  d9afd9ff-2ad1-7540-2749-9683f6ba8fbf
KCPL.MCDCI.ImpactRating.PCAmedium.enum  PCA (Medium)                            227a006a-ab50-fe86-5230-b7e37c7202c5
MCDCI_AssetStatus                       MCDCI Asset Status                      5105cfda-2ff9-5965-59cd-c0190ac37886
KCPL.MCDCI.DivisionGroup.Generation.... Generation                              7e454cb8-cfc4-b94a-b3c3-c593d7fe619f
KCPL.MCDCI.AssetStatus.Retired.enum     Retired                                 aac259e3-dfb3-a538-cc62-e349625cf804


Powershell Commands
Base Property IDs	Get-SCSMClassProperty MCDCI_Asset | ?{$_.Type -eq "MCDCI_DivisionGroup"} | fl
Get Enums		Get-SCSMEnumeration MCDCI
Get Enums Ids		Get-SCSMEnumeration MCDCI | select name, displayname, Id
Get SCSM Classes	Get-SCSMClass MCDCI | select Name, Displayname, Id
Get TypeProjections	Get-SCSMTypeProjection MCDCI | select  Name, Id
Get SRs			Get-SCSMClass System.WorkItem.ServiceRequest | Get-SCSMObject | ?{$_.Displayname -like "*SR25423*"}
Get RA			Get-SCSMClass System.WorkItem.Activity.ReviewActivity | Get-SCSMObject | ?{$_.Id -eq "RA25424"} | fl
Get Template Id			Get-SCSMObjectTemplate | ?{$_.Displayname -like 'KCPL Generation-TD*'} | fl

