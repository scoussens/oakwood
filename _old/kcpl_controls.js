/*
we are going to read the class = enum, 
iterate through the class and get the id, 
use the id to get the datasource,
then create an enum drop down off that.
this is a temprorary datasource
*/

//loads the selected list and setups up the onChange function
function loadList (item) {
	var projectionListId = $('#page-setup').data("projection-min");
	var projectionObjId = $('#page-setup').data("projection-full");
	
	var criteria = {
		"Id": projectionListId,
		"Criteria": {
			"Base": {
				"Expression": {
					"SimpleExpression": {
						"ValueExpressionLeft": {
							"GenericProperty": "Id"
						},
						"Operator": "Like",
						"ValueExpressionRight": {
							"Value": "%%"
						}
					}
				}
			}
		}
	}
	var stringdata = JSON.stringify(criteria);
	$.ajax({
		url: "/api/V3/Projection/GetProjectionByCriteria",
		data: stringdata,
		type: "POST",
		dataType: "json",
		contentType: 'application/json; charset=UTF-8',
		success: function (data) {
			function selectionChange (e) {
				var dataItem = this.dataItem(e.item);
				var criteria = {
					"Id": projectionObjId,
					"Criteria": {
						"Base": {
							"Expression": {
								"SimpleExpression": {
									"ValueExpressionLeft": {
										"GenericProperty": "Id"
									},
									"Operator": "Equal",
									"ValueExpressionRight": {
										"Value": dataItem.BaseId
									}
								}
							}
						}
					}
				};
				var stringdata = JSON.stringify(criteria);
				$.ajax({
					url: "/api/V3/Projection/GetProjectionByCriteria",
					data: stringdata,
					type: "POST",
					dataType: "json",
					contentType: 'application/json; charset=UTF-8',
					success: function (data) {
						if(typeof data === 'string') {
							console.log("No results returned or other error.");
							clearForm();
						} else {
							var jsonData = JSON.stringify(data[0]);
							ciModel = JSON.parse(jsonData);
							newModel = JSON.parse(jsonData);
							fillForm(ciModel);
						}
					}
				});
			};
			
			//creates the kendo list item
			$(item).kendoDropDownList({
				dataTextField: "DisplayName",
				dataValueField: "BaseId",
				dataSource: data,
				index: 0,
				suggest: true,
				filter: "contains",
				minLength: 3,
				optionLabel: {
					DisplayName: "Select a configuration item...",
					BaseId: ""
				},
				change: selectionChange,
				autoBind: true
			});
		}
	});
};
//end

function initEnum (item) {
	var name = $(item).attr("name");
	var enumId = $(item).data("id");
	
	$.ajax({
		url: "/api/V3/Enum/GetFlatList/" + enumId + "?itemFilter=&includeParents=true",
		data: "",
		type: "GET",
		dataType: "json",
		contentType: 'application/json; charset=UTF-8',
		success: function (data) {
			//function ties the changes to the datalist to the new object model
			function enumChange (e) {
				var dataItem = this.dataItem(e.item);
				
				if(newModel[name]) {
					newModel[name].Id = dataItem.Id;
					newModel[name].Name = dataItem.Name;
				}
				
				//check the global change array to see if this change has already been tracked, if not then track it
				if(changes.indexOf(name) < 0){
					changes.push(name);
				}
			}
			//creates the kendo list item
			$("input[name=" + name +"]").kendoDropDownList({
				dataTextField: "Text",
				dataValueField: "Id",
				dataSource: data,
				index: 0,
				change: enumChange
			});
		}
	});
}

function initRelationship (item) {
	var name = $(item).attr("name");
	var classId = $(item).data("class");
	var projectionId = $(item).data("projection");
	
	var criteria = {
		"Id": projectionId,
		"Criteria": {
			"Base": {
				"Expression": {
					"SimpleExpression": {
						"ValueExpressionLeft": {
							"GenericProperty": "Id"
						},
						"Operator": "Like",
						"ValueExpressionRight": {
							"Value": "%%"
						}
					}
				}
			}
		}
	}
	var stringdata = JSON.stringify(criteria);
	$.ajax({
		url: "/api/V3/Projection/GetProjectionByCriteria",
		data: stringdata,
		type: "POST",
		dataType: "json",
		contentType: 'application/json; charset=UTF-8',
		success: function (data) {
			//function ties the changes to the datalist to the new object model
			function relationshipChange (e) {
				var dataItem = this.dataItem(e.item);
				//check to see if this relationship exists in the model already and updates it
				if(newModel[name]) {
					newModel[name].BaseId = dataItem.BaseId;
					newModel[name].DisplayName = dataItem.DisplayName;
				} else if (newModel) { //if it doesn't exist yet (new item) then we create it on the model
					newModel[name] = {
						BaseId: dataItem.BaseId,
						DisplayName: dataItem.DisplayName
					};
				} else { //this is a catch if neither of the above works for some reason
					console.log("Error: Model does not exist.");
				}
				
				//check the global change array to see if this change has already been tracked, if not then track it
				if(changes.indexOf(name) < 0){
					changes.push(name);
				}
				
				//update any child relationship lists
				updateChildRelationships(name);
			}
			//creates the kendo list item
			$("input[name=" + name +"]").kendoDropDownList({
				dataTextField: "DisplayName",
				dataValueField: "BaseId",
				dataSource: data,
				index: 0,
				optionLabel: {
					DisplayName: "Select a related item...",
					BaseId: ""
				},
				change: relationshipChange
			});
		}
	});
}

//set the children relationships that relate to this object
function updateChildRelationships (parentName) {
	$("input[data-parent=" + parentName + "]").each(function () {
		var name = $(this).attr("name");
		var property = $(this).data("parent-property");
		var parentList = $("#" + parentName).data('kendoDropDownList');
		var selected = parentList.selectedIndex - 1
		
		if(selected >= 0 && parentList.dataSource._data[selected][property]){
			var relatedItems = parentList.dataSource._data[selected][property];
			var dataSource = new kendo.data.DataSource({
				data: relatedItems
			});
			dataSource.read();

			//creates the kendo list item
			var list = $("input[name=" + name +"]").data('kendoDropDownList');
			if(dataSource.total() > 0){
				list.setDataSource(dataSource);
				list.enable();
			};
		} else if(selected === -1) {
			var list = $("input[name=" + name +"]").data('kendoDropDownList');
			list.enable(0);	
		};		
	});
}

function initChildRelationship (item) {
	var name = $(item).attr("name");
	
	function relationshipChange (e) {
		var dataItem = this.dataItem(e.item);
		//check to see if this relationship exists in the model already and updates it
		if(newModel[name]) {
			newModel[name].BaseId = dataItem.BaseId;
			newModel[name].DisplayName = dataItem.DisplayName;
		} else if (newModel) { //if it doesn't exist yet (new item) then we create it on the model
			newModel[name] = {
				BaseId: dataItem.BaseId,
				DisplayName: dataItem.DisplayName
			};
		} else { //this is a catch if neither of the above works for some reason
			console.log("Error: Model does not exist.");
		}
		
		//check the global change array to see if this change has already been tracked, if not then track it
		if(changes.indexOf(name) < 0){
			changes.push(name);
		}
	}
	
	//creates the kendo list item
	$("input[name=" + name +"]").kendoDropDownList({
		autoBind: false,
		enable: false,
		dataTextField: "DisplayName",
		dataValueField: "BaseId",
		index: 0,
		optionLabel: {
			DisplayName: "Select a related item...",
			BaseId: ""
		},
		change: relationshipChange,
	});
}

//fills the form fields on selection of an asset
function fillForm(model) {
	$('#ci-update-form input').each(function (){
		var inputName = $(this).attr("name");
		var type = $(this).attr("type");
		
		if(type === "enum"){
			var dropList = $('input[name='+ inputName +']').data("kendoDropDownList");
			if(model[inputName]) {
				dropList.value(model[inputName].Id);
			} else {
				dropList.value('');
			}
		} else if (type === "relationship") {
			var dropList = $('input[name='+ inputName +']').data("kendoDropDownList");
			if(model[inputName]) {
				dropList.value(model[inputName].BaseId);
			} else {
				dropList.value('');
			}
		} else {
			$('input[name='+ inputName +']').val(model[inputName]);
		}
	});
}
//end

function initModel (form) {
	var formModel = {};
	
	formModel['ClassTypeId'] = $('#page-setup').data('class');
	$(form).find('input').each(function () {
		var name = $(this).attr('name');
		var type = $(this).attr('type');
		
		if(type === 'enum') {
			formModel[name] = {
				Id: null,
				Name: null
			}
		} else if (type === 'relationship') {
			formModel[name] = {
				BaseId: null,
				DisplayName: null,
				ClassId: $(this).data('class')
			}
		} else if (name != 'Submit') {
			formModel[name] = null;
		}
	});
	
	return formModel;
}
